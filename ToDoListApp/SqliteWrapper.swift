//
//  SqliteWrapper.swift
//  ToDoApp
//
//  Created by Sameer on 23/12/18.
//  Copyright © 2018 Lalit. All rights reserved.
//

import Foundation

class SqliteWrapper{

    
    static var databasePath:String=""
    static func copyDBFromBundleToDocument()
    {
        let appBundle = Bundle.main
        let sourceDBPath = appBundle.path(forResource: "items_list", ofType: "sqlite")
        let pathArray = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        SqliteWrapper.databasePath = pathArray[0]+"/items_list.sqlite"
        
    
        let fm = FileManager.default
        if (!fm.fileExists(atPath: SqliteWrapper.databasePath)) {
            do{
                try fm.copyItem(atPath: sourceDBPath!, toPath: SqliteWrapper.databasePath)
            }catch{
                print("error - ",error)
            }
        }
        print(SqliteWrapper.databasePath)
        
    }
    
    
    static func executeNonSelect (query : String){
        var databasePointer:OpaquePointer?=nil
        
        let result = sqlite3_open(SqliteWrapper.databasePath, &databasePointer)
        
        if(result == SQLITE_OK)
        {
            //execute query
            let queryResult = sqlite3_exec(databasePointer, query, nil, nil, nil)
            if (queryResult == SQLITE_OK) {
                print("query successfull")
            }else{
                print("error in query")
            }
        }
        sqlite3_close(databasePointer)
    }
    
    
    static func executeSelectQuery(query:String) ->
        [[String:String]]
    {
        var dataArray = [[String:String]]()
        
        
        var databasePointer: OpaquePointer? = nil
        let openResult = sqlite3_open(SqliteWrapper.databasePath, &databasePointer)
        
        if( openResult == SQLITE_OK )
        {
            
            let cquery = query.cString(using: String.Encoding.utf8)
            var resultStatement:OpaquePointer? = nil
            
            let queryResultCode = sqlite3_prepare(databasePointer, cquery, -1, &resultStatement, nil)
            if (queryResultCode == SQLITE_OK) {
                    while ( sqlite3_step(resultStatement) == SQLITE_ROW ) {
                    var rowDictionary = [String:String]()
                    let columnCount = sqlite3_column_count(resultStatement)
                    for i in 0...columnCount-1 {
                        let columnValue = sqlite3_column_text(resultStatement, Int32(i))
                        
                        let ptr = UnsafeRawPointer.init(columnValue)
                        let uptr = ptr?.bindMemory(to: CChar.self, capacity: 0)
                        let strValue = String(validatingUTF8: uptr!)!
                        
                        let colname = sqlite3_column_name(resultStatement, Int32(i))
                        let ptr1 = UnsafeRawPointer.init(colname)
                        let uptr1 = ptr1?.bindMemory(to: CChar.self, capacity: 0)
                        let strColName = String(validatingUTF8: uptr1!)!
                        
                        rowDictionary[strColName] = strValue
                    }
                    dataArray.append(rowDictionary)
                }
            }
        }
        sqlite3_close(databasePointer)
        return dataArray
    }
}
