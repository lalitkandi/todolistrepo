//
//  ViewController.swift
//  ToDoListApp
//
//  Created by Sameer on 24/12/18.
//  Copyright © 2018 Lalit. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBAction func addToList(_ sender: Any) {
        let showItemCon=self.storyboard!.instantiateViewController(withIdentifier: "additemviewcon") as! AddViewController
        self.present(showItemCon, animated: true, completion: nil)
        
    }
    @IBOutlet weak var addToList: UIButton!
    @IBOutlet weak var listTableView: UITableView!
    
    var listItemsArray = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        SqliteWrapper.copyDBFromBundleToDocument();
        let query = "select * from items"
        listItemsArray = SqliteWrapper.executeSelectQuery(query: query)
        let cellNib = UINib(nibName: "ItemTableViewCell", bundle: nil)
        listTableView.register(cellNib, forCellReuseIdentifier: "listcell")
        listTableView!.delegate = self
        listTableView!.dataSource = self
        listTableView.reloadData()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listTableView.dequeueReusableCell(withIdentifier: "listcell" , for: indexPath) as! ItemTableViewCell
        
        let itemDictionary = listItemsArray[indexPath.row]
        cell.itemName.text = itemDictionary["taskname"]
        cell.indexPath=indexPath
        cell.viewCon=self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let showItemCon=self.storyboard!.instantiateViewController(withIdentifier: "additemviewcon") as! AddViewController
        self.present(showItemCon, animated: true, completion: nil)
    }

}

