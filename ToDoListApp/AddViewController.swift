//
//  AddViewController.swift
//  ToDoListApp
//
//  Created by Sameer on 24/12/18.
//  Copyright © 2018 Lalit. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {

    @IBOutlet weak var itemName: UITextField!
    @IBOutlet weak var Comments: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SqliteWrapper.copyDBFromBundleToDocument()
    }
    
    @IBAction func clearAction(_ sender: Any) {
        itemName!.text = ""
        Comments!.text = ""
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        var name = itemName!.text
        var comments = Comments!.text
        
        let query = "insert into items(taskname,comments) values ('\(name)','\(comments)')"
        SqliteWrapper.executeNonSelect(query: query)
        
        itemName!.text = ""
        Comments!.text = ""
        
        
    }
    /*
    // MARK: - Navigation
     @IBAction func daleteAction(_ sender: Any) {
     }
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
